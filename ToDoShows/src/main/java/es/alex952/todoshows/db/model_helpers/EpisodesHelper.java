package es.alex952.todoshows.db.model_helpers;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

/**
 * Database helper class with common operations for Episodes.
 *
 * Created by alex952 on 15/07/13.
 */
public class EpisodesHelper {

    public static final String TAG = "EpisodesHelper";

    public static final String TABLE_NAME = "episodes";
    public static final String TABLE_PK = "_id";
    public static final String EPISODE_NAME = "name",
                               EPISODE_SEASON = "season",
                               EPISODE_NUMBER = "episode",
                               EPISODE_SYNOPSIS = "synopsis",
                               EPISODE_WATCHED = "watched";


    //TODO: Insert create and drop statements
    public static final String CREATE_STATEMENT = "";
    public static final String DROP_STATEMENT = "";


    //Methods to generate create and drop database executions
    public static void onCreate(SQLiteDatabase db) {
        Log.w(TAG, CREATE_STATEMENT);
        db.execSQL(CREATE_STATEMENT);
    }

    public static void onUpgrade(SQLiteDatabase db) {
        Log.w(TAG, "Upgrading database " + TABLE_NAME);
        db.execSQL(DROP_STATEMENT);
        onCreate(db);
    }

    public static SQLiteQueryBuilder getEpisode(Integer id) {
        SQLiteQueryBuilder q = new SQLiteQueryBuilder();

        q.setTables(TABLE_NAME);
        q.appendWhere(TABLE_PK + "=" + id);


        return q;
    }

    public static SQLiteQueryBuilder getEpisodes() {
        SQLiteQueryBuilder q = new SQLiteQueryBuilder();

        q.setTables(TABLE_NAME);

        return q;
    }
}
