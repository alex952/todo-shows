package es.alex952.todoshows.db.model_helpers;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Database helper class with common operations for Seasons.
 *
 * Created by alex952 on 15/07/13.
 */
public class SeasonsHelper {

    private static final String TAG = "SeasonsHelper";

    public static final String TABLE_NAME = "seasons";
    public static final String TABLE_PK = "_id";
    public static final String SEASON_NUMBER = "season",
                               SEASON_SHOW = "show";


    //TODO: Insert create and drop statements
    public static final String CREATE_STATEMENT = "";
    public static final String DROP_STATEMENT = "";


    //Methods to generate create and drop database executions
    public static void onCreate(SQLiteDatabase db) {
        Log.w(TAG, CREATE_STATEMENT);
        db.execSQL(CREATE_STATEMENT);
    }

    public static void onUpgrade(SQLiteDatabase db) {
        Log.w(TAG, "Upgrading database " + TABLE_NAME);
        db.execSQL(DROP_STATEMENT);
        onCreate(db);
    }


    //Database common methods for seasons
    public Cursor getSeasonInfo(Integer id) {
        return null;
    }

    public Cursor getSeasonEpisodes() {
        return null;
    }
}