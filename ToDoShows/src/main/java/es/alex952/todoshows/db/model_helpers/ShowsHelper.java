package es.alex952.todoshows.db.model_helpers;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Database helper class with common operations for Shows.
 *
 * Created by alex952 on 15/07/13.
 */
public class ShowsHelper {

    private static final String TAG = "ShowsHelper";

    public static final String TABLE_NAME = "shows";
    public static final String TABLE_PK = "_id";
    public static final String SHOWS_NAME = "name",
                               SHOWS_YEAR = "year";


    //TODO: Insert create and drop statements
    public static final String CREATE_STATEMENT = "";
    public static final String DROP_STATEMENT = "";


    //Methods to generate create and drop database executions
    public static void onCreate(SQLiteDatabase db) {
        Log.w(TAG, CREATE_STATEMENT);
        db.execSQL(CREATE_STATEMENT);
    }

    public static void onUpgrade(SQLiteDatabase db) {
        Log.w(TAG, "Upgrading database " + TABLE_NAME);
        db.execSQL(DROP_STATEMENT);
        onCreate(db);
    }


    //Database common methods for shows
    public Cursor getShow(Integer id) {
        return null;
    }

    public Cursor getShows() {
        return null;
    }
}
