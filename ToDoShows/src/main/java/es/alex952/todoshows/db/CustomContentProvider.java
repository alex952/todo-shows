package es.alex952.todoshows.db;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import es.alex952.todoshows.db.model_helpers.EpisodesHelper;

/**
 * Defines URI rules to fetch and insert data by the application
 *
 * Created by alex952 on 16/07/13.
 */
public class CustomContentProvider extends ContentProvider {

    //Database object
    private DatabaseHelper mDB;



    //Content URI example: content://es.alex952.todoshows.data.Provider/episodes

    //Authority of the ContentProvider
    private static final String AUTHORITY = "es.alex952.todoshows.data.Provider";
    //Base URI
    private static final String EPISODES_BASE_PATH = "episodes";

    //Identifiers for the matcher
    public static final int EPISODES = 100;
    public static final int EPISODES_ID = 110;

    //Final URI scheme
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + EPISODES_BASE_PATH);

    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/shows";
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/shows";

    //Declaration and static initialization of the url matching patters
    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        //content://es.alex952.todoshows.data.Provider/episodes
        sURIMatcher.addURI(AUTHORITY, EPISODES_BASE_PATH, EPISODES);
        //content://es.alex952.todoshows.data.Provider/episodes/1
        sURIMatcher.addURI(AUTHORITY, EPISODES_BASE_PATH + "/#", EPISODES_ID);
    }

    @Override
    public boolean onCreate() {
        this.mDB = new DatabaseHelper(getContext());

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder q = null;
        int uriType = sURIMatcher.match(uri);

        switch (uriType) {
            case EPISODES_ID:
                q = EpisodesHelper.getEpisode(Integer.parseInt(uri.getLastPathSegment()));
                break;

            case EPISODES:
                q = EpisodesHelper.getEpisodes();
                break;

            default:
                throw new IllegalArgumentException("Unknown URI");
        }

        Cursor c = q.query(mDB.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
