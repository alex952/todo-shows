package es.alex952.todoshows.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import es.alex952.todoshows.db.model_helpers.EpisodesHelper;
import es.alex952.todoshows.db.model_helpers.SeasonsHelper;
import es.alex952.todoshows.db.model_helpers.ShowsHelper;

/**
 * Implements common methods to create and upgrade
 * the database.
 *
 * Created by alex952 on 15/07/13.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private final String TAG = "DatabaseHelper";

    public DatabaseHelper(Context context) {
        super(context, DatabaseInfo.DB_NAME, null, DatabaseInfo.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.w(TAG, "Generating database for application ToDoShows");

        ShowsHelper.onCreate(db);
        SeasonsHelper.onCreate(db);
        EpisodesHelper.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion);

        ShowsHelper.onUpgrade(db);
        SeasonsHelper.onUpgrade(db);
        EpisodesHelper.onUpgrade(db);
    }
}
