package es.alex952.todoshows.db;

import es.alex952.todoshows.db.model_helpers.ShowsHelper;

/**
 * Created by alex952 on 15/07/13.
 */
public class DatabaseInfo {
    //Database information
    public static final String DB_NAME = "ToDoShows";
    public static final int DB_VERSION = 1;
}
