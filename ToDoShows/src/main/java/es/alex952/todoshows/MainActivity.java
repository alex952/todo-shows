package es.alex952.todoshows;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.database.Cursor;
import android.os.Bundle;
import android.app.Activity;
import android.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.Menu;

import es.alex952.todoshows.db.CustomContentProvider;
import es.alex952.todoshows.db.model_helpers.EpisodesHelper;

public class MainActivity extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String [] from = { EpisodesHelper.EPISODE_NAME };

        int [] to = { R.id.episode_name };

        getLoaderManager().initLoader(0, null, this);
        CursorAdapter ca = new SimpleCursorAdapter(getApplicationContext(),
                R.layout.episodes_lisititem,
                null,
                from,
                to,
                0);

        setListAdapter(ca);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public android.content.Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String [] projection = {EpisodesHelper.TABLE_PK, EpisodesHelper.EPISODE_NAME };

        CursorLoader cl = new CursorLoader(this, CustomContentProvider.CONTENT_URI, projection, null, null, null);

        return cl;
    }

    @Override
    public void onLoadFinished(android.content.Loader<Cursor> cursorLoader, Cursor cursor) {

    }

    @Override
    public void onLoaderReset(android.content.Loader<Cursor> cursorLoader) {

    }
}
